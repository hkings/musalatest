package com.musala.software.enums;

public enum Model {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight
}