package com.musala.software.Repositories;

import com.musala.software.model.Drone;
import com.musala.software.model.Medication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepository extends CrudRepository<Medication, Long> {
   // List<Medication> findByDroneId(Long droneId);

}
