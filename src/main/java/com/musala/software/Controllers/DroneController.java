package com.musala.software.Controllers;

import com.musala.software.model.Drone;
import com.musala.software.model.Medication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.musala.software.Services.DroneService;

import java.util.List;
import java.util.Optional;

@RestController
public class DroneController {


    @Autowired
    private DroneService droneService;

    public DroneController(DroneService droneService) {
        this.droneService = droneService;
    }

    @PostMapping("/drones")
    public ResponseEntity<?> registerDrone(@RequestBody Drone drone) {
        try {
            droneService.registerDrone(drone);
            return ResponseEntity.ok().body(drone);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/drones/{droneSerialNumber}/medications")
    public ResponseEntity<?> loadMedications(@PathVariable String droneSerialNumber, @RequestBody List<Medication> medications) {
        try {
            droneService.loadDrone(droneSerialNumber, medications);
            return ResponseEntity.ok().body(medications);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/drones/{droneSerialNumber}/medications")
    public ResponseEntity<?> getLoadedMedications(@PathVariable String droneSerialNumber) {
        try {
            List<Medication> loadedMedications = droneService.getDroneMedications(droneSerialNumber);
            return ResponseEntity.ok().body(loadedMedications);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/drones/available")
    public ResponseEntity<?> getAvailableDrones() {
        try {
            List<Drone> availableDrones = droneService.getAvailableDrones();
            return ResponseEntity.ok().body(availableDrones);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/helloworld")
    public String HelloWorld(){
        return "Hello World";
    }

    @GetMapping("/drones/{droneSerialNumber}/battery")
    public ResponseEntity<?> getDroneBatteryLevel(@PathVariable String droneSerialNumber) {
        try {
            int batteryLevel = droneService.getDroneBatteryLevel(droneSerialNumber);
            return ResponseEntity.ok().body(batteryLevel + " % remaining");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }


    }



}

