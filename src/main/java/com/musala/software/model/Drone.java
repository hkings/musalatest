package com.musala.software.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;



@Entity
@Table(name = "drones")
public class Drone {

    @Id
    @NotBlank(message = "Serial number is mandatory")
    @Size(max = 100, message = "Serial number must be at most 100 characters")
    @Column(name = "serial_number")
    private String serialNumber;
    @NotNull(message = "Model is mandatory")
    private String model;
    @NotNull(message = "Weight limit is mandatory")
    @Max(value = 500, message = "Weight limit must not exceed 500 grams")
    @Column(name = "weight_limit")
    private int weightLimit;
    @Min(value = 0, message = "Battery capacity must not be negative")
    @Max(value = 100, message = "Battery capacity must not exceed 100")
    @Column(name = "battery_capacity")
    private int batteryCapacity;
    @NotNull(message = "State is mandatory")
    private String state;
    @OneToMany
    @JoinTable(
            name = "drone_medication",
            joinColumns = @JoinColumn(name = "serial_number" ),
            inverseJoinColumns = {@JoinColumn(name = "medication_code", referencedColumnName = "code"), @JoinColumn(name = "medication_name", referencedColumnName = "name")}
    )
    private List<Medication> loadedMedications;

    public Drone(String serialNumber, String model, int weightLimit, int batteryCapacity, String state) {
        this.serialNumber = serialNumber;
        this.model = model;
        this.weightLimit = weightLimit;
        this.batteryCapacity = batteryCapacity;
        this.state = state;
        this.loadedMedications = new ArrayList<>();
    }

    public Drone() {
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(int weightLimit) {
        this.weightLimit = weightLimit;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<Medication> getLoadedMedications() {
        return loadedMedications;
    }

    public void setLoadedMedications(List<Medication> loadedMedications) {
        this.loadedMedications = loadedMedications;
    }
}
