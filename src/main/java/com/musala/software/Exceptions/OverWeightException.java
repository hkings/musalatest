package com.musala.software.Exceptions;

public class OverWeightException extends RuntimeException {
    public OverWeightException(String message) {
        super(message);
    }
}