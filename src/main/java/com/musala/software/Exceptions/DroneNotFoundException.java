package com.musala.software.Exceptions;

public class DroneNotFoundException extends RuntimeException {
    public DroneNotFoundException(String id) {
        super("Could not find drone " + id);
    }
}