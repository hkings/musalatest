package com.musala.software.Exceptions;

public class DroneStateException extends RuntimeException {
    public DroneStateException(String message) {
        super(message);
    }
}