package com.musala.software.Services.impl;

import com.musala.software.Exceptions.DroneNotFoundException;
import com.musala.software.Exceptions.DroneStateException;
import com.musala.software.Exceptions.OverWeightException;
import com.musala.software.Services.DroneService;
import com.musala.software.enums.DroneState;
import com.musala.software.model.Drone;
import com.musala.software.model.Medication;
import org.springframework.stereotype.Service;
import com.musala.software.Repositories.DroneRepository;
import com.musala.software.Repositories.MedicationRepository;

import java.util.*;

@Service
public class DroneServiceImpl implements DroneService {

    private DroneRepository droneRepository;
    private MedicationRepository medicationRepository;

    public DroneServiceImpl(DroneRepository droneRepository, MedicationRepository medicationRepository) {
        this.droneRepository = droneRepository;
        this.medicationRepository = medicationRepository;

    }

    private final Map<String, Drone> drones = new HashMap<>();
        private final List<Medication> medications = new ArrayList<>();

        // Register a new drone
        public void registerDrone(Drone drone) {
            droneRepository.save(drone);

        }

    public void loadDrone(String serialNumber, List<Medication> medications) throws DroneNotFoundException, DroneStateException, OverWeightException {

        Optional<Drone> optionalDrone = droneRepository.findBySerialNumber(serialNumber);

        if (!optionalDrone.isPresent()) {
            throw new DroneNotFoundException("Drone not found with ID: " + serialNumber);
        }

        Drone drone = optionalDrone.get();

        if (drone.getBatteryCapacity() < 25) {
            throw new DroneStateException("Drone battery level is less than 25%. Current level: " + drone.getBatteryCapacity());
        }

        double totalWeight = medications.stream().mapToDouble(Medication::getWeight).sum();
        if (totalWeight > drone.getWeightLimit()) {
            throw new OverWeightException("Total weight of medications exceeds the weight limit of the drone. Weight limit: " + drone.getWeightLimit() + "g");
        }

        drone.setState(DroneState.LOADING.name());
        droneRepository.save(drone);

        for (Medication medication : medications) {
            medication.setDrone(drone);
            drone.getLoadedMedications().add(medication);
            medicationRepository.save(medication);
        }

        drone.setState(DroneState.LOADED.name());
        droneRepository.save(drone);
    }

        // Get medications loaded on a drone
        public List<Medication> getDroneMedications(String serialNumber) throws DroneNotFoundException {
            Optional<Drone> optionalDrone = droneRepository.findBySerialNumber(serialNumber);
            if (!optionalDrone.isPresent()) {
                throw new DroneNotFoundException("Drone not found with ID: " + serialNumber);
            }
            return optionalDrone.get().getLoadedMedications();

        }

    public List<Drone> getAvailableDrones() {
       List<Drone> aa =  droneRepository.findByState(DroneState.IDLE.name());
        return aa;
    }

    public int getDroneBatteryLevel(String serialNumber) throws DroneNotFoundException {
        Optional<Drone> optionalDrone = droneRepository.findBySerialNumber(serialNumber);
        if (!optionalDrone.isPresent()) {
            throw new DroneNotFoundException("Drone not found with ID: " + serialNumber);
        }

        return optionalDrone.get().getBatteryCapacity();
    }
    }


