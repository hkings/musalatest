package com.musala.software.Services;

import com.musala.software.Repositories.DroneRepository;

import com.musala.software.model.Drone;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class DroneBatteryTask {

    private static final Logger logger = LoggerFactory.getLogger(DroneBatteryTask.class);

    @Autowired
    private DroneRepository droneRepository;

    @Scheduled(fixedRate = 60000) // execute every minute
    public void checkBatteryLevels() {
        List<Drone> drones = (List<Drone>) droneRepository.findAll();
        for (Drone drone : drones) {
            if (drone.getBatteryCapacity() < 25) {
                // log event that drone battery is low
                String message = "Drone " + drone.getSerialNumber() + " has low battery level.";
                logEvent(message);
            }
        }
    }

    private void logEvent(String message) {
        logger.error(message);
    }

}
