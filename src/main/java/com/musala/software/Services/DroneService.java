package com.musala.software.Services;

import com.musala.software.Exceptions.DroneNotFoundException;
import com.musala.software.model.Drone;
import com.musala.software.model.Medication;

import java.util.List;
import java.util.Optional;

public interface DroneService {

    int getDroneBatteryLevel(String serialNumber) throws DroneNotFoundException;
    void registerDrone(Drone drone);
    void loadDrone(String serialNumber, List<Medication> medications);
    List<Medication> getDroneMedications(String serialNumber);
    List<Drone> getAvailableDrones();



}
