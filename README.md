# Musala Software test



## Run the app

Option 1) 
run the app with IDE

Option 2)
./gradlew bootrun

## notes 
logs in /logs folder

DB : localhost:8080/h2-console/login.jsp
jdbc url : jdbc:h2:mem:testdb
username : sa

## API

### registering a drone
```

POST
http://localhost:8080/drones
headers: Content-Type = application/json
payload example :
{  
    "serialNumber": "123456788",  
    "model": "Lightweight",  
    "weightLimit": 502,  
    "state": "IDLE",
    "batteryCapacity": 20

}   
```

### loading a drone with medication items
```

POST
http://localhost:8080/drones/123456788/medications
headers: Content-Type = application/json
payload example :
[
    {
        "name": "Aspirin",
        "weight": 100,
        "code": "ASP001",
        "image": "https://example.com/aspirin.jpg"
    },
    {
        "name": "Ibuprofen",
        "weight": 150,
        "code": "IBP002",
        "image": "https://example.com/ibuprofen.jpg"
    },
    {
        "name": "Acetaminophen",
        "weight": 200,
        "code": "ACE003",
        "image": "https://example.com/acetaminophen.jpg"
    }
]}   
```
### checking loaded medication items for a given drone
```

GET
http://localhost:8080/drones/123456789/medications
headers: Content-Type = application/json

```
### checking available drones for loading
```

GET
http://localhost:8080/drones/available
headers: Content-Type = application/json
```

### check drone battery level for a given drone
```

GET
http://localhost:8080/drones/123456789/battery
headers: Content-Type = application/json
```

